from flask import Flask, request, abort, jsonify
from flask.globals import session
from XL_3L_Trong_rau import*
from XL_API_Weather import*


DichVu = Flask(__name__, static_url_path="/Media", static_folder="Media")
DichVu.secret_key = "QuocNguyenViet" 

#--biến dùng chung

#--Xử lý biến cố khơi động
@DichVu.route("/", methods =["GET"])
def XL_KhoiDong():  
    khungHTML = docKhungHTML()
    Danh_sach_Loai_rau = Xu_ly_Tao_Danh_sach_Rau_Cong_ty()
    chuoiHTML = Chuoi_HTML_Danh_sach_Loai_rau_Rau(Danh_sach_Loai_rau)
    chuoiHTML = khungHTML.replace("chuoiHTML", chuoiHTML)     
    return chuoiHTML
#--Xu ly bien co XL_Tim_theo_Ma_Loai_rau
@DichVu.route("/loai-rau", methods =["GET"])
def XL_Tim_theo_Ma_Loai_rau():
    khungHTML = docKhungHTML()
    Ma_Loai_rau = request.args["Ma_Loai_rau"]
    Danh_sach_Loai_rau = Xu_ly_Tao_Danh_sach_Rau_Cong_ty(Ma_Loai_rau)
    chuoiHTML = Chuoi_HTML_Danh_sach_Loai_rau_Rau(Danh_sach_Loai_rau)
    chuoiHTML = khungHTML.replace("chuoiHTML", chuoiHTML)     
    return chuoiHTML
#--Xu ly bien co Dang nhap nguoi trong rau
@DichVu.route("/xl-dang-nha-nguoi-trong-rau", methods =["POST"])
def XL_Dang_nhap_Nguoi_trong_rau():
    Nguoi_trong_rau = {}
    khungHTML = Doc_Khung_HTML_Nguoi_trong_rau() 
    Ten_dang_nhap = request.form["txtTenDangNhap"]
    Mat_khau = request.form["txtMatKhau"]
    Nguoi_trong_rau = Xu_ly_Dang_nhap_Nguoi_trong_rau(Ten_dang_nhap, Mat_khau)
    if Nguoi_trong_rau == {}:
        chuoiHTML = "<div class='bg-danger'><h1>Dang nhap khong hop le</h1></div>"
        chuoiHTML = khungHTML.replace("chuoiHTML", chuoiHTML)
    else:
        session["Nguoi_dung"] = Nguoi_trong_rau
        chuoiHTML = Chuoi_HTML_Nguoi_trong_rau_Dang_nhap(Nguoi_trong_rau)
        chuoiHTML = khungHTML.replace("chuoiHTML", chuoiHTML)   
    return chuoiHTML
#--Xu ly bien co data-DHT-json
@DichVu.route("/data-DHT-json", methods =["GET"])
def XL_Data_DHT_JSON():
    DHT = {}
    DHT = Xu_ly_Lay_Thong_tin_Thoi_tiet()
   # print(DHT)
    return DHT

#--Xu ly bien co data-API-Thoi_tiet
@DichVu.route("/data-API-Thoi-tiet", methods =["GET"])
def XL_Data_API_Thoi_tiet(): 
    DHT = {}
    DHT = Xu_ly_Lay_API_Thoi_tiet()
    print(DHT)
    return DHT
#--/Xu ly bien co data-DHT-json
#--Xu ly bien co data-API-Thoi_tiet
@DichVu.route("/Du-bao-Thoi-tiet", methods =["GET"])
def XL_Du_bao_Thoi_tiet(): 
    DHT = {}
    DHT = Xu_ly_Lay_API_Thoi_tiet()
    ChuoiHTML_Du_bao_Thoi_tiet = Chuoi_HTML_Du_bao_Thoi_tiet(DHT)
    return ChuoiHTML_Du_bao_Thoi_tiet
#--/Xu ly bien co data-DHT-json

#--Xu ly bien co lay gia nong san
@DichVu.route("/xem-gia-nong-san", methods =["GET"])
def XL_Xem_gia_nong_san():
    khungHTML = docKhungHTML()
    Danh_sach_Gia_rau = Doc_Danh_sach_gia()
    chuoiHTML = Chuoi_HTML_Gia_rau(Danh_sach_Gia_rau)
    chuoiHTML = khungHTML.replace("chuoiHTML", chuoiHTML)     
    return chuoiHTML
#--/Xu ly bien co lay gia nong san

#--Xu ly bien co tim kiem nong san
@DichVu.route("/tim-kiem-gia-nong-san", methods =["POST"])
def XL_Tim_kiem_gia_nong_san():
    khungHTML = docKhungHTML()
    Danh_sach_Gia_rau = Doc_Danh_sach_gia()
    TuKhoa = request.form["txtTimKiem"]
    Danh_sach_Tim_kiem = Xu_ly_TimKiem_Gia_rau(Danh_sach_Gia_rau,TuKhoa)
    chuoiHTML = Chuoi_HTML_Gia_rau(Danh_sach_Tim_kiem, TuKhoa)
    chuoiHTML = khungHTML.replace("chuoiHTML", chuoiHTML)     
    return chuoiHTML
#--/Xu ly bien co tim kiem nong san

#--Xu_ly_Auto_Complete
@DichVu.route("/api-gia-rau")
def XL_Auto_Complete_Gia_rau():
    Danh_sach_rau = Doc_Danh_sach_gia() 
    Danh_sach_Ten_rau = [] 
    for rau in Danh_sach_rau:
        Danh_sach_Ten_rau.append(rau["Ten"])  
    return jsonify({"Ten": Danh_sach_Ten_rau})

#--/Xu_ly_Auto_Complete
#Xử lý biến cố thoát đăng nhập
@DichVu.route("/Nguoi-trong-rau/thoat-dang-nhap", methods=["GET"])
def XL_ThoatDangNhap():
    khungHTML = docKhungHTML()
    chuoiHTML = ""
    if "Nguoi_dung" in session:
        session.pop("Nguoi_dung", None)    
        return XL_KhoiDong()
#--Run chuong trinh
if __name__ == '__main__':
   DichVu.run()