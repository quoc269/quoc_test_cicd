# Python program to find current
# weather details of any city
# using openweathermap api
 
# import required modules
import requests
import json
def Xu_ly_Lay_Thong_tin_Thoi_tiet():
# Enter your API key here
    
    Thong_so_DHT = {}
    # base_url variable to store url
    base_url = "http://api.openweathermap.org/data/2.5/weather?"
    
    
    # complete_url variable to store
    # complete url address
    complete_url = base_url + "lat=10.69&lon=107.029&appid=eba388452a0bf30a47dc42910c833ef6"
    
    # get method of requests module
    # return response object
    response = requests.get(complete_url)
    
    # json method of response object
    # convert json format data into
    # python format data
    x = response.json()
    
    # Now x contains list of nested dictionaries
    # Check the value of "cod" key is equal to
    # "404", means city is found otherwise,
    # city is not found
    if x["cod"] != "404":
    
        # store the value of "main"
        # key in variable y
        y = x["main"]
    
        # store the value corresponding
        # to the "temp" key of y
        current_temperature = y["temp"]
    
        # store the value corresponding
        # to the "pressure" key of y
        current_pressure = y["pressure"]
    
        # store the value corresponding
        # to the "humidity" key of y
        current_humidity = y["humidity"]
    
        # store the value of "weather"
        # key in variable z
        z = x["weather"]
        
        # store the value corresponding
        # to the "description" key at
        # the 0th index of z
        weather_description = z[0]["description"]
        Do_C_Tu_F = current_temperature - 273.15
        Thong_so_DHT ={"temp":Do_C_Tu_F,"humi":current_humidity,"pressure": current_pressure}
       # Thong_so_DHT = [{"temp":Do_C_Tu_F}, {"humi":current_humidity}] 
    return Thong_so_DHT

#---Xu ly API MOI TRUONG
def Xu_ly_Lay_API_Thoi_tiet():
# Enter your API key here
    
    Thong_so_API = {}
    # base_url variable to store url
    base_url = "https://api.openweathermap.org/data/2.5/onecall?"
    
    
    # complete_url variable to store
    # complete url address
    complete_url = base_url + "lat=10.69&lon=107.02&exclude=hourly,daily&appid=eba388452a0bf30a47dc42910c833ef6"
    
    # get method of requests module
    # return response object
    response = requests.get(complete_url)
    
    # json method of response object
    # convert json format data into
    # python format data
    x = response.json()
    
    # Now x contains list of nested dictionaries
    # Check the value of "cod" key is equal to
    # "404", means city is found otherwise,
    # city is not found
    
    
      
    y = x["current"]    
    current_temperature = y["temp"]   
    current_pressure = y["pressure"] 
    current_humidity = y["humidity"]    
    current_cloud = y["clouds"]
    current_uvi = y["uvi"]
    current_wind_speed = y["wind_speed"]
    current_weather = y["weather"][0]
    current_main = current_weather["main"]
    current_description = current_weather["description"]
    current_icon = current_weather["icon"]
    Do_C_Tu_F = current_temperature - 273.15
    Thong_so_API = {"uvi":current_uvi, "cloud":current_cloud, "wind_speed":current_wind_speed,"main": current_main, "des":current_description, "icon":current_icon }
       
    return Thong_so_API
